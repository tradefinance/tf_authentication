var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var jwt = require('jsonwebtoken');
var _ = require('lodash');
var async = require('async');
var Boom = require('boom');

var verifyJWT = function(authorizationHeader, customerId, db, callback){

    console.log("In verifyJWT");
    if (_.isEmpty(authorizationHeader)){
        console.log("No token found");
        var error = new Error("No Access Token found")
        return callback(error, null)
    }
    var token = authorizationHeader.split(' ')[1];
    var decoded = jwt.decode(token);
    var app_short_name = decoded.iss;

            db.collection('customers', function(error, collection){
                if(error){
                    return callback(error, {isValid: false})
                }
                else{
                    collection.findOne({_id: ObjectID(customerId)}, function(e, item){
                        if(e){
                            return callback(e, {isValid: false});
                        }
                        else{
                            if(item.keys && item.keys.length > 0){
                                var validKey = _.find(item.keys, function(key){
                                    return key.app_short_name === app_short_name;
                                })

                                if(validKey){
                                    jwt.verify(token, validKey.api_key, function(err, decoded){
                                        if (err){
                                            return callback(err, {isValid: false, message: 'Token not valid'});
                                        }
                                        else{
                                            return callback(err, {isValid: true, claims: decoded, message: 'Token Successfully Validated'});
                                        }
                                    });
                                } else{
                                    return callback(e, {isValid: false, message: 'API Key Not Found'})
                                }
                            } else{
                                return callback(e, {isValid: false, message: 'Customer has no API Keys'})
                            }
                        }
                    })
                }
            })
}

var scheme = function (server, options){
    return {
        authenticate: function(request, reply){
            async.auto({
                verifyJWT: function(callback){
                    verifyJWT(request.headers.authorization, request.params.customer_id,
                            server.plugins['hapi-mongodb'].db, callback)
                }
            }, function(err, result){
                if(err){
                    console.dir(err);
                    return reply(Boom.unauthorized('Error authorizing request'), null, {credentials: null });
                }

                if(!result.verifyJWT.isValid){
                    return reply(Boom.unauthorized(result.verifyJWT.message), null, {credentials: null});
                }

                return reply.continue({credentials: result.verifyJWT.claims})
            })
        }
    }
}

var getClaims = function(authHeader){
    var token = autHeader.split(' ')[1];
    var decoded = jwt.decode(token);
    return decoded;
}

module.exports = {
    scheme : scheme,
    getClaims : getClaims
}